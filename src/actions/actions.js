/*
    Типы действий
 */
export const SET_CUR_MENU_ITEM = 'SET_CUR_MENU_ITEM';
export const SET_IS_LOGIN = 'SET_IS_LOGIN';

/*
    Генераторы действий
 */
export function setCurMenuItem(curMenuItem) {
    return {
        type: SET_CUR_MENU_ITEM,
        curMenuItem
    }
}

export function setIsLogin(isLogin) {
    return {
        type: SET_IS_LOGIN,
        isLogin
    }
}