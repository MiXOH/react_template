import { combineReducers } from 'redux';
import { SET_CUR_MENU_ITEM, SET_IS_LOGIN } from './../actions/actions';

import MainPage from '../components/main/mainPage/MainPage';
import ShowRegistry from '../containers/ShowRegistry';
import RegistrySuccess from '../components/main/registry/RegistrySuccess';
import ShowSignIn from '../containers/ShowSignIn';
import ShowProfile from '../containers/ShowProfile';
import RestorePass from '../components/main/restorePass/RestorePass';
import PageNotFound from '../components/main/pageNotFound/PageNotFound';


/*
    name - имя пункта меню
    component - компонент, который отображать для данного пункта меню
    isLogin - отображать пункт для залогиненых пользователей
    isNotLogin - отображать пункт для не залогиненых пользователей
 */
const menuList = [
    {
        id: 1,
        title: 'Home',
        component: MainPage,
        isLogin: true,
        isNotLogin: true
    },
    {
        id: 2,
        title: 'Sign In',
        component: ShowSignIn,
        isLogin: false,
        isNotLogin: true
    },
    {
        id: 3,
        title: 'Registry',
        component: ShowRegistry,
        isLogin: false,
        isNotLogin: true
    },
    {
        id: 4,
        title: 'Profile',
        component: ShowProfile,
        isLogin: true,
        isNotLogin: false
    },
    {
        id: 5,
        title: 'Restore Pass',
        component: RestorePass,
        isLogin: false,
        isNotLogin: false
    },
    {
        id: 6,
        title: 'Page Not Found',
        component: PageNotFound,
        isLogin: false,
        isNotLogin: false
    },
    {
        id: 7,
        title: 'Registry Success',
        component: RegistrySuccess,
        isLogin: false,
        isNotLogin: false
    },
];

function menu(state = menuList, action) {
    return state;
}

function curMenuItem(state = 1, action) {
    switch (action.type) {
        case SET_CUR_MENU_ITEM:
            return action.curMenuItem;
        default:
            return state;
    }
}

function isLogin(state = false, action) {
    switch (action.type) {
        case SET_IS_LOGIN:
            return action.isLogin;
        default:
            return state;
    }
}

const mainApp = combineReducers({
    menu,
    curMenuItem,
    isLogin
});

export default mainApp;