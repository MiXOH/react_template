import { createStore } from 'redux';
import mainApp from './../reducers/reducers';

export default function configureStore() {
    const store = createStore(mainApp);

    return store;
}

