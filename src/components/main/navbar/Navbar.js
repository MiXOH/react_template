import React from 'react';

import NavbarItem from './NavbarItem';

export default class Navbar extends React.Component {
    render() {
        const menuList = this.props.menu;
        const onItemClick = this.props.onItemClick;

        // Формируем список к выводу
        const menu = menuList.map((menuItem) => <NavbarItem key={menuItem.id} item={menuItem} onClickMenu={() => onItemClick(menuItem.id)}/>);

        return (
            <div>
                <nav>
                    <ul className="navbar-nav mr-4">
                        {menu}
                    </ul>
                </nav>
            </div>
        );
    }
}

