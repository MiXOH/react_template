import React from 'react';

export default class NavbarItem extends React.Component {
    render() {
        const item = this.props.item;
        const onClickMenu = this.props.onClickMenu;
        return (
            <li className="nav-item">
                <a className="nav-link" href="#" onClick={onClickMenu}>{item.title}</a>
            </li>
        );
    }
}
