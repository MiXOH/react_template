import React from 'react';

export default class Profile extends React.Component {
    render() {
        const onLogout = this.props.onLogout;

        return (
            <div>
                Profile

                <button onClick={onLogout}>Sign Out</button>
            </div>
        )
    }
}