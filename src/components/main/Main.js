import React from 'react';

import ShowNavbar from '../../containers/ShowNavbar';

export default class Main extends React.Component {
    render() {
        const curMenu = this.props.curMenu;

        const BodyComponent = curMenu.component;

        return (
            <div>
                <ShowNavbar/>
                <BodyComponent/>
            </div>
        )
    }
}
