import React from 'react';

export default class SignIn extends React.Component {
    constructor() {
        super();
        this.state = {
            login: '',
            pass: ''
        }
    }

    onChangeLogin = (e) => {
        this.setState({login: e.target.value});
    }

    onChangePass = (e) => {
        this.setState({pass: e.target.value});
    }

    render() {
        const onLogin = this.props.onLogin;
        const onRestorePass = this.props.onRestorePass;
        const login = this.state.login;
        const pass = this.state.pass;

        return(
            <div>
                <div>
                    Login: <input onChange={this.onChangeLogin}/>
                </div>
                <div>
                    Password: <input onChange={this.onChangePass}/>
                </div>
                <div>
                    <button onClick={() => onLogin(login, pass)}>Sign In</button>
                </div>
                <div>
                    <button onClick={onRestorePass}>Restore password</button>
                </div>
            </div>
        )
    }
}