import React from 'react';

export default class RegistryForm extends React.Component {
    constructor() {
        super();

        this.state = {
            name: '',
            email: '',
            password: ''
        }
    }

    onChangeName = (e) => {
        this.setState({name: e.target.value});
    }

    onChangeEmail = (e) => {
        this.setState({email: e.target.value});
    }

    onChangePassword = (e) => {
        this.setState({password: e.target.value});
    }

    render() {
        const onRegistry = this.props.onRegistry;
        const name = this.state.name;
        const email = this.state.email;
        const password = this.state.password;

        return (
            <div>
                <div>
                    Name: <input onChange={this.onChangeName}/>
                </div>
                <div>
                    Mail: <input onChange={this.onChangeEmail}/>
                </div>
                <div>
                    Password: <input onChange={this.onChangePassword}/>
                </div>
                <div>
                    <button onClick={() => onRegistry(name, email, password)}>Registry</button>
                </div>
            </div>
        );
    }
}
