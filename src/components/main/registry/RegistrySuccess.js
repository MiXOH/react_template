import React from 'react';

export default class RegistrySuccess extends React.Component {
    render() {
        return (
            <div>
                Registry is success!
                Please check your email!
            </div>
        )
    }
}