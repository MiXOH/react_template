import React from 'react';

export default class PageNotFound extends React.Component {
    render() {
        return (
            <div>
                Sorry, but your page was not found :(
            </div>
        )
    }
}