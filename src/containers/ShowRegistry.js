import { connect } from 'react-redux';
import { setCurMenuItem } from '../actions/actions';
import RegistryForm from '../components/main/registry/RegistryForm';

const mapDispatchToProps = (dispatch) => {
    return {
        onRegistry: (name, email, password) => {
            // Тут делаем отправку нового пользователя на сервер (api/registry)
            dispatch(setCurMenuItem(7));
        },
    }
}

const ShowRegistry = connect(
    null,
    mapDispatchToProps
)(RegistryForm)

export default ShowRegistry;