import { connect } from 'react-redux';
import { setIsLogin, setCurMenuItem } from '../actions/actions';
import Profile from '../components/main/profile/Profile';

const mapDispatchToProps = (dispatch) => {
    return {
        onLogout: () => {
            dispatch(setIsLogin(false));
            dispatch(setCurMenuItem(2));
        },
    }
}

const ShowProfile = connect(
    null,
    mapDispatchToProps
)(Profile)

export default ShowProfile;