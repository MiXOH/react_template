import { connect } from 'react-redux';
import { setCurMenuItem } from '../actions/actions';
import Navbar from '../components/main/navbar/Navbar';

const getActualMenu = (menu, isLogin) => {
    return menu.filter(item => ((isLogin && item.isLogin) || (!isLogin && item.isNotLogin)))
}

const mapStateToProps = (state) => {
    return {
        menu: getActualMenu(state.menu, state.isLogin)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onItemClick: (item) => {
            dispatch(setCurMenuItem(item));
        }
    }
}

const ShowNavbar = connect(
    mapStateToProps,
    mapDispatchToProps
)(Navbar)

export default ShowNavbar;