import { connect } from 'react-redux';
import Main from '../components/main/Main';

const getCurMenu = (menu, cur) => {
    const itemMenu = menu.find(item => item.id === cur);
    if (itemMenu === undefined) {
        return menu.find(item => item.id === 6);
    }
    else {
        return itemMenu;
    }
}

const mapStateToProps = (state) => {
    return {
        curMenu: getCurMenu(state.menu, state.curMenuItem)
    }
}

const ShowMain = connect(
    mapStateToProps
)(Main);

export default ShowMain;