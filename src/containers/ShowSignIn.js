import { connect } from 'react-redux';
import { setIsLogin, setCurMenuItem } from '../actions/actions';
import SignIn from '../components/main/signIn/SignIn';

const USERS = [
    {
        login: '123',
        pass: '123'
    },
    {
        login: 'userX',
        pass: 'passX'
    }
];

const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (login, pass) => {
            let isLogin = false;
            USERS.forEach((user) => {
                if ((user.login === login) && (user.pass === pass)) {
                    isLogin = true;
                }
            });
            dispatch(setIsLogin(isLogin));
            dispatch(setCurMenuItem(4));
        },
        onRestorePass: () => {
            dispatch(setCurMenuItem(5));
        }
    }
}

const ShowSignIn = connect(
    null,
    mapDispatchToProps
)(SignIn)

export default ShowSignIn;