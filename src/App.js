import React from 'react';
import './App.css';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import mainApp from './reducers/reducers';

import ShowMain from './containers/ShowMain';

let store = createStore(mainApp);

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <ShowMain/>
            </Provider>
        );
    }
}

export default App;
